﻿//C# Example
using UnityEditor;
using UnityEngine;

public class MyWindow : EditorWindow
{
	
	public static string currentIdioma;
	public static bool SelecciónIdioma;
	public static bool SelecciónIdioma_2;
	public static bool Seguridad;
	public static bool AulaOption;
	public static string ipAula;

	public static string _appName;
	public static string _bundleApp;
	
	// Add menu item named "My Window" to the Window menu
	[MenuItem("Window/My Window")]
	public static void ShowWindow()
	{


		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(MyWindow));
	}

	void Update(){
		currentIdioma = GameObject.FindObjectOfType<Pantalla>().IdiomaActual;
		SelecciónIdioma = GameObject.FindObjectOfType<Pantalla>().SelLangOption;
		SelecciónIdioma_2 = GameObject.FindObjectOfType<Reporte>()._checkLanguage;
		Seguridad = GameObject.FindObjectOfType<Simulador>().SeguridadSwitch;
		AulaOption = GameObject.FindObjectOfType<Simulador>().ModoAula;
		ipAula = GameObject.FindObjectOfType<Simulador>().url_aula;

		_appName = PlayerSettings.productName;
		_bundleApp = PlayerSettings.applicationIdentifier;
	}

	void OnGUI()
	{
		GUILayout.Label ("Global Vars", EditorStyles.boldLabel);
		
		currentIdioma = EditorGUILayout.TextField ("Current Idioma", currentIdioma);
		
		SelecciónIdioma = EditorGUILayout.Toggle ("Selección Idioma", SelecciónIdioma);
		SelecciónIdioma_2 = EditorGUILayout.Toggle ("Selección Idioma check", SelecciónIdioma_2);
		Seguridad = EditorGUILayout.Toggle ("Seguridad", Seguridad);
		AulaOption = EditorGUILayout.Toggle ("AulaOption", AulaOption);
		
		ipAula = EditorGUILayout.TextField ("ipAula", ipAula);
		
		_appName = EditorGUILayout.TextField ("_appName", _appName);
		_bundleApp = EditorGUILayout.TextField ("_bundleApp", _bundleApp);
		
		//groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
		//myBool = EditorGUILayout.Toggle ("Toggle", myBool);
		//myFloat = EditorGUILayout.Slider ("Slider", myFloat, -3, 3);
		//EditorGUILayout.EndToggleGroup ();
	}
}