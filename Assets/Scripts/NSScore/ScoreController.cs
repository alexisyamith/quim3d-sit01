﻿#pragma warning disable
using System;
using System.Globalization;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;

namespace NSScore
{
    public class ScoreController : MonoBehaviour
    {
        #region members

        private XmlDocument xmlDocumentAula;

        private float minimalSuccessScore = 0.6f;

        private float minScoreValue;

        private float maxScoreValue;

        private QualificationElement[] arrayScoresAlphabetic;

        private string urlScore;

        private TypeScore typeScore = TypeScore.None;

        private XmlNode scoreTypeXmlNode;

        [SerializeField] private float scoreRangeDefault = 5f;

        [SerializeField] private UnityEvent OnScoreSuccess;

        [SerializeField] private UnityEvent OnScoreFailed;
        #endregion

        #region properties

        public XmlDocument XMLDocumentAula
        {
            set
            {
                xmlDocumentAula = value;
                
                if (xmlDocumentAula != null)
                    LoadScoreConfiguration();
            }
        }
        #endregion

        #region methods

        public void LoadScoreConfiguration()
        {
            scoreTypeXmlNode = xmlDocumentAula.SelectSingleNode("/data/general/score");

            if (scoreTypeXmlNode != null)
            {
                minimalSuccessScore = float.Parse(scoreTypeXmlNode.Attributes["minimalSuccessScore"].Value);
                LoadScoreNumberRange();
                LoadScoreAlphabeticRange();
            }
            else
                Debug.LogWarning("El nodo score no existe en el archivo aula XML", this);
        }

        private void LoadScoreNumberRange()
        {
            var tmpNumericNode = scoreTypeXmlNode.SelectSingleNode("numeric");

            if (tmpNumericNode.Attributes["selected"].Value == "true")
            {
                typeScore = TypeScore.Numeric;
                minScoreValue = float.Parse(tmpNumericNode.ChildNodes[0].InnerText);
                maxScoreValue = float.Parse(tmpNumericNode.ChildNodes[1].InnerText);
            }
        }

        private void LoadScoreAlphabeticRange()
        {
            var tmpAlphabeticNode = scoreTypeXmlNode.SelectSingleNode("alphabetical");

            if (tmpAlphabeticNode.Attributes["selected"].Value == "true")
            {
                typeScore = TypeScore.Alphabetic;
                arrayScoresAlphabetic = new QualificationElement[tmpAlphabeticNode.ChildNodes.Count];

                for (int i = 0; i < tmpAlphabeticNode.ChildNodes.Count; i++)
                {
                    arrayScoresAlphabetic[i].name = tmpAlphabeticNode.ChildNodes[i].InnerText;
                    arrayScoresAlphabetic[i].maxValue = float.Parse(tmpAlphabeticNode.ChildNodes[i].Attributes["max"].Value);
                }
            }
        }

        public string GetScoreFromFactor01(float argFactorScore01)
        {
            var tmpFinalScore = string.Empty;

            if (typeScore == TypeScore.Numeric)
                tmpFinalScore = ComputeFinalScoreWithNumbersRange(argFactorScore01);
            else if (typeScore == TypeScore.Alphabetic)
                tmpFinalScore = ComputeFinalScoreWithAlphanumeric(argFactorScore01);
            else
                tmpFinalScore = ComputeFinalScoreWithDefaultRange(argFactorScore01);
            
            ExecuteEventOfScoreSuccess(argFactorScore01);
            return tmpFinalScore;
        }

        private string ComputeFinalScoreWithNumbersRange(float argFactorScore01)
        {
            var tmpScoreScaled = argFactorScore01 * (maxScoreValue - minScoreValue) + minScoreValue;
            tmpScoreScaled = Mathf.Round(tmpScoreScaled * 10f) / 10f;
            
            var tmpFinalScore = tmpScoreScaled.ToString(CultureInfo.InvariantCulture);
            tmpFinalScore += "/" + maxScoreValue;
            return tmpFinalScore;
        }

        private string ComputeFinalScoreWithAlphanumeric(float argFactorScore01)
        {
            string tmpFinalScore = String.Empty;

            for (int i = 0; i < arrayScoresAlphabetic.Length; i++)
                if (argFactorScore01 < arrayScoresAlphabetic[i].maxValue)
                {
                    tmpFinalScore = arrayScoresAlphabetic[i].name;
                    break;
                }

            return tmpFinalScore;
        }

        private string ComputeFinalScoreWithDefaultRange(float argFactorScore01)
        {
            var tmpFinalScore = (scoreRangeDefault * argFactorScore01).ToString("0.0", CultureInfo.InvariantCulture);
            return tmpFinalScore;
        }

        private void ExecuteEventOfScoreSuccess(float argFactorScore01)
        {
            if (argFactorScore01 >= minimalSuccessScore)
                OnScoreSuccess.Invoke();
            else
                OnScoreFailed.Invoke();
        }
        #endregion
    }

    public enum TypeScore
    {
        None,
        Numeric,
        Alphabetic
    }

    [Serializable]
    public struct QualificationElement
    {
        public string name;

        public float maxValue;
    }
}