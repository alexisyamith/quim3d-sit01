﻿using System.Collections.Generic;

namespace Extras
{
    /// <summary>
    /// Clase que contiene los diccionarios que se usan paralas traducciones
    /// </summary>
	public class Diccionario
    {
        public static Dictionary<string, string> espanol_Ingles = new Dictionary<string, string>();
        public static Dictionary<string, string> portugues_Ingles = new Dictionary<string, string>();
        public static Dictionary<string, string> espanol_Portugues = new Dictionary<string, string>();

        public static void DictEspaIngles()
        {
            espanol_Ingles.Add("visualizar", "Visualize");
            espanol_Ingles.Add("fijar temperatura", "SET TEMPERATURE");
            espanol_Ingles.Add("solución", "Solution");
            espanol_Ingles.Add("solución (l)", "Solution (L)");
            espanol_Ingles.Add("close app", "CLOSE APP");
            espanol_Ingles.Add("salir", "EXIT");
            espanol_Ingles.Add("ácido (l)", "Acid (L)");
            espanol_Ingles.Add("ácido:", "Acid:");
            espanol_Ingles.Add("base (l)", "Base (L)");
            espanol_Ingles.Add("base:", "Base:");
            espanol_Ingles.Add("temperatura (°c)", "Temperature (°C)");
            espanol_Ingles.Add("agitador (rpm)", "Agitator (RPM)");
            espanol_Ingles.Add("parámetros", "PARAMETERS");
            espanol_Ingles.Add("azúcar (l)", "Sugar (L)");
            espanol_Ingles.Add("azúcar:", "Sugar:");
            espanol_Ingles.Add("levadura (l)", "Yeast (L)");
            espanol_Ingles.Add("levadura:", "Yeast:");
            espanol_Ingles.Add("solución fermentada (l)", "Fermented solution (L)");
            espanol_Ingles.Add("solución a (ml)", "Solution A (ml)");
            espanol_Ingles.Add("solución b (ml)", "Solution B (ml)");
            espanol_Ingles.Add("solución c (ml)", "Solution C (ml)");
            espanol_Ingles.Add("solución d (ml)", "Solution D (ml)");
            espanol_Ingles.Add("solvente (ml)", "Solvent (ml)");
            espanol_Ingles.Add("texto", "text");
            espanol_Ingles.Add("usuario", "User");
            espanol_Ingles.Add("usuario:", "User:");
            espanol_Ingles.Add("tiempo dispuesto", "Available time");
            espanol_Ingles.Add("tiempo destilación", "Distillation time");
            espanol_Ingles.Add("condensador agua", "Water condenser");
            espanol_Ingles.Add("nivel espuma)", "Foam level");
            espanol_Ingles.Add("alcohol destilado (%)", "Distilled alcohol (%)");
            espanol_Ingles.Add("ph", "pH");
            espanol_Ingles.Add("indicadores", "Indicators");
            espanol_Ingles.Add("alcohol etílico (l)", "Ethyl alcohol (L)");
            espanol_Ingles.Add("oxígeno", "Oxygen");
            espanol_Ingles.Add("prueba de  síntesis (%)", "Synthesis test (%)");
            espanol_Ingles.Add("proceso aminoácido (ml)", "Amino acid process (ml)");
            espanol_Ingles.Add("acondicionamiento mp (ml)", "Conditioning MR (ml)");
            espanol_Ingles.Add("presión (pa)", "Pressure (PA)");
            espanol_Ingles.Add("alcohol (%)", "Alcohol (%)");
            espanol_Ingles.Add("tiempo fermentación", "Fermentation time");
            espanol_Ingles.Add("nivel cultivo", "Cultivation level");
            espanol_Ingles.Add("pantalla digital", "Digital display");
            espanol_Ingles.Add("termine el actual ejercicio", "End the current simulation");
            espanol_Ingles.Add("iniciar", "START");
            espanol_Ingles.Add("reto", "Challenge");
            espanol_Ingles.Add("práctica libre", "Free practice");
            espanol_Ingles.Add("evaluación", "Evaluation");
            espanol_Ingles.Add("terminar", "FINISH");
            espanol_Ingles.Add("reintentar", "RETRY");
            espanol_Ingles.Add("continuar", "CONTINUE");
            espanol_Ingles.Add("calculadora", "Calculator");
            espanol_Ingles.Add("ecuaciones", "Equations");
            espanol_Ingles.Add("gráfico", "Graph");
            espanol_Ingles.Add("instrucciones", "Instructions");
            espanol_Ingles.Add("reiniciar", "Reset");
            espanol_Ingles.Add("procedimiento", "Procedure");
            espanol_Ingles.Add("donde", "Where");
            espanol_Ingles.Add("alcohol vs tiempo", "Alcohol vs. Time");
            espanol_Ingles.Add("alcohol", "alcohol");
            espanol_Ingles.Add("líquido a destilar", "liquid to distill");
            espanol_Ingles.Add("v alcohol", "V alcohol");
            espanol_Ingles.Add("presión", "Pressure:");
            espanol_Ingles.Add("v líquido a destilar", "V liquid to distill");
            espanol_Ingles.Add("v alcohol: volumen destilado de alcohol.", "V alcohol: volume of alcohol distilled.");
            espanol_Ingles.Add("v líquido a destilar: volumen solución fermentada a destilar.", "V liquid to distilled: volume distilling fermented solution.");
            espanol_Ingles.Add("% alcohol líquido a destilar: porcentaje de alcohol a destilar.", " % alcohol liquid to distillate: percent of alcohol to distillate.");
            espanol_Ingles.Add("% a/sol = porcentaje de a en la solución.", "% a/sol= percent of a in the solution");
            espanol_Ingles.Add("generar reporte", "Generate report");
            espanol_Ingles.Add("datos de simulación", "Simulation data");
            espanol_Ingles.Add("tiempo de sesión:", "Time:");
            espanol_Ingles.Add("tiempo", "Time:");
            espanol_Ingles.Add("tiempo:", "Time:");
            espanol_Ingles.Add("n° intentos:", "Attempt:");
            espanol_Ingles.Add("va = volumen de a.", "Va = volume of a.");
            espanol_Ingles.Add("vsol = volumen de solución.", "Vsol = volume of solution.");
            espanol_Ingles.Add("nota: la suma de los porcentajes de los componentes de una solución es 100.", "Note: The sum of the percentages of the components in a solution is 100.");
            espanol_Ingles.Add("medio(levaduras)", "Medium (yeast)");
            espanol_Ingles.Add("glucosa", "Glucose");
            espanol_Ingles.Add("gas carbónico", "Carbon Dioxide");
            espanol_Ingles.Add("alcohol etílico o etanol", "Ethyl alcohol or ethanol");
            espanol_Ingles.Add("temperatura vs tiempo", "Temperature vs. time");
            espanol_Ingles.Add("laboratorio de química", "3D CHEMISTRY LAB");
            espanol_Ingles.Add("institución:", "Institution:");
            espanol_Ingles.Add("institución", "Institution");
            espanol_Ingles.Add("unidad:", "Unit:");
            espanol_Ingles.Add("intentos:", "Attempts:");
            espanol_Ingles.Add("alcoholes, aldehídos y cetonas.", "Alcohols, aldehydes and ketones.");
            espanol_Ingles.Add("fecha:", "Date:");
            espanol_Ingles.Add("curso:", "Course:");
            espanol_Ingles.Add("curso", "Course");
            espanol_Ingles.Add("id curso", "Course ID");//Course ID
            espanol_Ingles.Add("id curso:", "Course ID:");
            espanol_Ingles.Add("situación:", "Situation:");
            espanol_Ingles.Add("preparación de una mezcla fermentada al 45% en alcohol", "Preparation of a fermented mixture to 45% alcohol");
            espanol_Ingles.Add("tabla de datos", "Data table");
            espanol_Ingles.Add("preguntas de evaluación", "Evaluation questions");
            espanol_Ingles.Add("preguntas complementarias", "Complementary questions");
            espanol_Ingles.Add("resuelva las siguientes preguntas, anéxelas con la guía de aprendizaje a este reporte y envíelos a su profesor.", "Solve the following questions and add them to the learning guide on this report and send to your teacher.");
            espanol_Ingles.Add("enviar", "SEND");
            espanol_Ingles.Add("visualizar pdf", "PDF View");
            espanol_Ingles.Add("situación", "Situation");
            espanol_Ingles.Add("practica libre", "Free practice");
            espanol_Ingles.Add("estación 3:", "STATION 3:");
            espanol_Ingles.Add("reactor síntesis de aminoácidos", "AMINO ACID SYNTHESIS REACTOR");
            espanol_Ingles.Add("estación 1: fermentador", "STATION 1: FERMENTER");
            espanol_Ingles.Add("estación 2:  destilador", "STATION 2: DISTILLER");
            espanol_Ingles.Add("puede ir a generar el reporte de esta práctica o realizar un nuevo intento. haga clic en aceptar para continuar.", "You can go to generate the report of this practice or perform a new attempt. Click OK to continue.");
            espanol_Ingles.Add("aceptar", "OK");
            espanol_Ingles.Add("¡felicitaciones! ha completado el reto propuesto, haga clic en el botón continuar para generar el reporte de laboratorio.", "Congratulations! You have completed the proposed challenge. Click “Continue” to generate the laboratory report.");
            espanol_Ingles.Add("azúcar", "Sugar");
            espanol_Ingles.Add("levadura", "Yeast");
            espanol_Ingles.Add("ácido", "Acid");
            espanol_Ingles.Add("temperatura (°c )", "Temperature (°C)");
            espanol_Ingles.Add("agitador", "Agitator");
            espanol_Ingles.Add("alcohol etílico l", "Ethyl Alcohol L");
            espanol_Ingles.Add("alcohol etílico %", "Ethyl Alcohol %");
            espanol_Ingles.Add("solución fermentada", "Fermented Solution");
            espanol_Ingles.Add("alcohol destilado l", "Distilled Alcohol L");
            espanol_Ingles.Add("alcohol destilado %", "Distilled Alcohol %");
            espanol_Ingles.Add("nivel de agua", "Water Level");
            espanol_Ingles.Add("solución a", "Solution A");
            espanol_Ingles.Add("solución b", "Solution B");
            espanol_Ingles.Add("solución c", "Solution C");
            espanol_Ingles.Add("solución d", "Solution D");
            espanol_Ingles.Add("solución:", "Solution:");
            espanol_Ingles.Add("solvente", "Solvent");
            espanol_Ingles.Add("proceso aminoácido", "Amino Process");
            espanol_Ingles.Add("prueba de síntesis", "Synthesis test");
            espanol_Ingles.Add("cond. agua", "Water condi.");
            espanol_Ingles.Add("válvula de alivio", "Relief valve");
            espanol_Ingles.Add("prueba final", "Final Test");
            espanol_Ingles.Add("proceso de comprobación", "Verification process");
            espanol_Ingles.Add("agitar", "Shake");
            espanol_Ingles.Add("azucar", "Sugar");
            espanol_Ingles.Add("temperatura", "Temperature");
            espanol_Ingles.Add("pantalla", "Screen");
            espanol_Ingles.Add("apagado", "Off");
            espanol_Ingles.Add("encendido", "On");
            espanol_Ingles.Add("química", "Chemistry");
            espanol_Ingles.Add("solución fermentada (L)", "Fermented solution (L)");
            espanol_Ingles.Add("máxima capacidad combinada de líquidos y gases. active la válvula de alivio si es necesario o termine el proceso.", "Maximum capacity of combined liquids and gases. Activate the relief valve if necessary, or finish the process.");
            espanol_Ingles.Add("antes de ingresar insumos desactive la válvula de alivio.", "Before to add the solutions  deactivate  the relief valve.");
            espanol_Ingles.Add("debes reiniciar, revisa los parametros", "You must restart. Check the data are parameters.");
            espanol_Ingles.Add("error en las rpm el proceso se demorara 30 minutos mas.", "Wrong Value in RPM, you must restart the process.");
            espanol_Ingles.Add("primero debe encender el agitador.", "You must turn the agitator first.");
            espanol_Ingles.Add("la solución aún no esta disponible.", "The solution is not yet available.");
            espanol_Ingles.Add("las soluciones ya se encuentran disponibles.", "The solutions are already available");
            espanol_Ingles.Add("no se puede agregar mas contenido.", "You cannot add more content.");
            espanol_Ingles.Add("alto", "High");
            espanol_Ingles.Add("bajo", "Low");
            espanol_Ingles.Add("medio", "Medium");
            espanol_Ingles.Add("(v)", "(T)");
            espanol_Ingles.Add("quigen", "GENCHEM");

            espanol_Ingles.Add("calificación:", "Grade:");

            espanol_Ingles.Add("el tanque actualmente se encuentra lleno. si es necesario, active la válvula de escape.", "The tank is currently full. If necessary, activate the exhaust valve.");
            espanol_Ingles.Add("el tanque actualmenta se encuentra vacío.", "The tank is currently empty.");

            espanol_Ingles.Add("v", "T");
            //módulo de seguridad
            espanol_Ingles.Add("cancelar", "Cancel");
            espanol_Ingles.Add("validar", "Validate");
            espanol_Ingles.Add("intentos", "Attempts");
            espanol_Ingles.Add("regresar", "BACK");
            espanol_Ingles.Add("inicio de sesión", "LOGIN");
            espanol_Ingles.Add("ingresar","START");
            espanol_Ingles.Add("reporte", "REPORT");
        }

        public static void DictPor_Ingles()
        {

        }

        public static void DictEspa_Por()
        {
            espanol_Portugues.Add("visualizar", "Visualize");
            espanol_Portugues.Add("fijar temperatura", "Ajustar temperatura");
            espanol_Portugues.Add("solución", "Solução");
            espanol_Portugues.Add("solución (l)", "Solução (L)");
            espanol_Portugues.Add("close app", "Sair da aplicação");
            espanol_Portugues.Add("salir", "SAIR");
            espanol_Portugues.Add("ácido (l)", "Ácido (L)");
            espanol_Portugues.Add("ácido:", "Ácido:");
            espanol_Portugues.Add("base (l)", "Base (L)");
            espanol_Portugues.Add("base:", "Base:");
            espanol_Portugues.Add("temperatura (°c)", "Temperatura (°C)");
            espanol_Portugues.Add("agitador (rpm)", "Agitador (RPM)");
            espanol_Portugues.Add("parámetros", "PARÁMETROS");
            espanol_Portugues.Add("azúcar (l)", "Açúcar (L)");
            espanol_Portugues.Add("azúcar:", "Açúcar:");
            espanol_Portugues.Add("levadura (l)", "Levedura (L)");
            espanol_Portugues.Add("levadura:", "Levedura:");
            espanol_Portugues.Add("solución fermentada (l)", "Solução fermentada (L))");
            espanol_Portugues.Add("solución a (ml)", "Solution A (ml)");
            espanol_Portugues.Add("solución b (ml)", "Solution B (ml)");
            espanol_Portugues.Add("solución c (ml)", "Solution C (ml)");
            espanol_Portugues.Add("solución d (ml)", "Solution D (ml)");
            espanol_Portugues.Add("solvente (ml)", "Solvente (ml)");
            espanol_Portugues.Add("texto", "Texto");
            espanol_Portugues.Add("usuario", "Usuário");
            espanol_Portugues.Add("usuario:", "Usuário:");
            espanol_Portugues.Add("tiempo dispuesto", "Tempo disposto");
            espanol_Portugues.Add("tiempo destilación", "Tempo destilação");
            espanol_Portugues.Add("condensador agua", "Condensador de água");
            espanol_Portugues.Add("nivel espuma)", "Nível espuma");
            espanol_Portugues.Add("alcohol destilado (%)", "Álcool destilado (%)");
            espanol_Portugues.Add("ph", "pH");
            espanol_Portugues.Add("indicadores", "Indicadores");
            espanol_Portugues.Add("alcohol etílico (l)", "Álcool etílico (L)");
            espanol_Portugues.Add("oxígeno", "Oxigênio");
            espanol_Portugues.Add("prueba de  síntesis (%)", "Teste de síntese (%)");
            espanol_Portugues.Add("proceso aminoácido (ml)", "Processo aminoácido (ml)");
            espanol_Portugues.Add("acondicionamiento mp (ml)", "Acondicionamento MP (mL)");
            espanol_Portugues.Add("presión (pa)", "Pressão (PA)");
            espanol_Portugues.Add("alcohol (%)", "Álcool (%)");
            espanol_Portugues.Add("tiempo fermentación", "Tempo fermentação");
            espanol_Portugues.Add("nivel cultivo", "Nível cultura");
            espanol_Portugues.Add("pantalla digital", "Tela display");
            espanol_Portugues.Add("termine el actual ejercicio", "Termine o exercício atual");
            espanol_Portugues.Add("iniciar", "Iniciar");
            espanol_Portugues.Add("reto", "Desafio");
            espanol_Portugues.Add("práctica libre", "Prática livre");
            espanol_Portugues.Add("evaluación", "Avaliação");
            espanol_Portugues.Add("terminar", "TERMINAR");
            espanol_Portugues.Add("reintentar", "TENTAR DE NOVO");
            espanol_Portugues.Add("continuar", "CONTINUAR");
            espanol_Portugues.Add("calculadora", "Calculadora");
            espanol_Portugues.Add("ecuaciones", "Equações");
            espanol_Portugues.Add("gráfico", "Gráfico");
            espanol_Portugues.Add("instrucciones", "Instruções");
            espanol_Portugues.Add("reiniciar", "Reiniciar");
            espanol_Portugues.Add("procedimiento", "Procedimento");
            espanol_Portugues.Add("donde", "Onde");
            espanol_Portugues.Add("alcohol vs tiempo", "Álcool vs tempo");
            espanol_Portugues.Add("alcohol", "álcool");
            espanol_Portugues.Add("líquido a destilar", "Solução a destilar");
            espanol_Portugues.Add("v alcohol", "V álcool");
            espanol_Portugues.Add("presión", "Pressão:");
            espanol_Portugues.Add("v líquido a destilar", "V Solução a destilar");
            espanol_Portugues.Add("v alcohol: volumen destilado de alcohol.", "V álcool: Volume destilado de álcool.");
            espanol_Portugues.Add("v líquido a destilar: volumen solución fermentada a destilar.", "V líquido a ser destilado: Volume solução fermentada a ser destilado.");
            espanol_Portugues.Add("% alcohol líquido a destilar: porcentaje de alcohol a destilar.", "% álcool/liquido a ser destilado: porcentagem de álcool destilador.");
            espanol_Portugues.Add("% a/sol = porcentaje de a en la solución.", "% a/sol= Porcentagem de A na solução");
            espanol_Portugues.Add("generar reporte", "Gerar relatório");
            espanol_Portugues.Add("datos de simulación", "Dados da simulação");
            espanol_Portugues.Add("tiempo de sesión:", "Tempo:");
            espanol_Portugues.Add("tiempo", "Tempo:");
            espanol_Portugues.Add("tiempo:", "Tempo:");
            espanol_Portugues.Add("n° intentos:", "Nº tentativas:");
            espanol_Portugues.Add("va = volumen de a.", "Va = Volume de A.");
            espanol_Portugues.Add("vsol = volumen de solución.", "Vsol = Volume de solução.");
            espanol_Portugues.Add("nota: la suma de los porcentajes de los componentes de una solución es 100.", "Note: A soma das porcentagens dos componentes de uma solução é 100.");
            espanol_Portugues.Add("medio(levaduras)", "Meio (leveduras)");
            espanol_Portugues.Add("glucosa", "Glicose");
            espanol_Portugues.Add("gas carbónico", "gás carbônico");
            espanol_Portugues.Add("alcohol etílico o etanol", "álcool etílico ou etanol");
            espanol_Portugues.Add("temperatura vs tiempo", "Temperatura vs Tempo");
            espanol_Portugues.Add("laboratorio de química", "Laboratório 3D de química");
            espanol_Portugues.Add("institución:", "Instituição:");
            espanol_Portugues.Add("institución", "Instituição");
            espanol_Portugues.Add("unidad:", "Unidade:");
            espanol_Portugues.Add("intentos:", "Tentativas:");
            espanol_Portugues.Add("intentos", "Tentativas");
            espanol_Portugues.Add("alcoholes, aldehídos y cetonas.", "Álcoois, aldeídos e cetonas.");
            espanol_Portugues.Add("fecha:", "Data:");
            espanol_Portugues.Add("curso:", "Curso:");
            espanol_Portugues.Add("curso", "Curso");
            espanol_Portugues.Add("id curso", "ID do curso");//Course ID
            espanol_Portugues.Add("id curso:", "ID do curso:");
            espanol_Portugues.Add("situación:", "Situação:");
            espanol_Portugues.Add("preparación de una mezcla fermentada al 45% en alcohol", "Preparo de uma mistura fermentada a 45% em álcool");
            espanol_Portugues.Add("tabla de datos", "Tabela de dados");
            espanol_Portugues.Add("preguntas de evaluación", "Avaliação");
            espanol_Portugues.Add("preguntas complementarias", "Perguntas norteadoras");
            espanol_Portugues.Add("resuelva las siguientes preguntas, anéxelas con la guía de aprendizaje a este reporte y envíelos a su profesor.", "Resolva as seguintes perguntas, e depois anexe na guia de aprendizagem do relatório e envie a seu professor.");
            espanol_Portugues.Add("enviar", "Enviar");
            espanol_Portugues.Add("visualizar pdf", "Visualizar PDF");
            espanol_Portugues.Add("situación", "Situação");
            espanol_Portugues.Add("practica libre", "Prática livre");
            espanol_Portugues.Add("estación 3:", "ESTAÇÃO 3:");
            espanol_Portugues.Add("reactor síntesis de aminoácidos", "REATOR SÍNTESE DE AMINOÁCIDOS DESAFIO");
            espanol_Portugues.Add("estación 1: fermentador", "ESTAÇÃO 1: FERMENTADOR");// FERMENTADOR DESAFIO
            espanol_Portugues.Add("estación 2:  destilador", "ESTAÇÃO 2: DESTILADOR");// DESTILADOR. DESAFIO
            espanol_Portugues.Add("puede ir a generar el reporte de esta práctica o realizar un nuevo intento. haga clic en aceptar para continuar.", "Você pode ir para gerar o relatório desta prática ou fazer uma nova tentativa. Clique no relatório para continuar.");
            espanol_Portugues.Add("aceptar", "ACEITAR");
            espanol_Portugues.Add("¡felicitaciones! ha completado el reto propuesto, haga clic en el botón continuar para generar el reporte de laboratorio.", "Parabéns! Você concluiu o desafio proposto, clique no botão continuar para gerar o relatório da experiência.");
            espanol_Portugues.Add("azúcar", "Açúcar");
            espanol_Portugues.Add("levadura", "Levedura");
            espanol_Portugues.Add("ácido", "Ácido");
            espanol_Portugues.Add("temperatura (°c )", "Temperatura (°C)");
            espanol_Portugues.Add("agitador", "Agitator");
            espanol_Portugues.Add("alcohol etílico l", "álcool etílico L");
            espanol_Portugues.Add("alcohol etílico %", "álcool etílico %");
            espanol_Portugues.Add("solución fermentada", "Solução fermentada (L)");
            espanol_Portugues.Add("alcohol destilado l", "Álcool destilado L");
            espanol_Portugues.Add("alcohol destilado %", "Álcool destilado (%)");
            espanol_Portugues.Add("nivel de agua", "Nível de água");
            espanol_Portugues.Add("solución a", "Solução A");
            espanol_Portugues.Add("solución b", "Solução B");
            espanol_Portugues.Add("solución c", "Solução C");
            espanol_Portugues.Add("solución d", "Solução D");
            espanol_Portugues.Add("solución:", "Solução:");
            espanol_Portugues.Add("solvente", "Solvente");
            espanol_Portugues.Add("proceso aminoácido", "Processo aminoácido");
            espanol_Portugues.Add("prueba de síntesis", "Teste de síntese");
            espanol_Portugues.Add("cond. agua", "Cond. Água");
            espanol_Portugues.Add("válvula de alivio", "Válvula de segurança");
            espanol_Portugues.Add("prueba final", "Teste final");
            espanol_Portugues.Add("proceso de comprobación", "Processo comprovação");
            espanol_Portugues.Add("agitar", "Agitar");
            espanol_Portugues.Add("azucar", "Açúcar");
            espanol_Portugues.Add("temperatura", "Temperatura");
            espanol_Portugues.Add("pantalla", "Tela");
            espanol_Portugues.Add("apagado", "Off");
            espanol_Portugues.Add("encendido", "On");
            espanol_Portugues.Add("química", "Química");
            espanol_Portugues.Add("solución fermentada (L)", "Solução fermentada (L)");
            espanol_Portugues.Add("máxima capacidad combinada de líquidos y gases. active la válvula de alivio si es necesario o termine el proceso.", "Máxima capacidade combinada de líquidos e gases. Ative a válvula de segurança ou reinicie o processo, se necessário.");
            espanol_Portugues.Add("antes de ingresar insumos desactive la válvula de alivio.", "Antes de entrar nos suprimentos, desative a válvula de segurança.");

            espanol_Portugues.Add("debes reiniciar, revisa los parametros", "Você deve reiniciar, revise os parâmetros.");
            espanol_Portugues.Add("error en las rpm el proceso se demorara 30 minutos mas.", "Erro RPM o processo levará mais de 30 minutos.");
            espanol_Portugues.Add("primero debe encender el agitador.", "Primeiro você deve ligar o agitador.");
            espanol_Portugues.Add("la solución aún no esta disponible.", "A solução ainda não está disponível.");
            espanol_Portugues.Add("las soluciones ya se encuentran disponibles.", "As soluções já estão disponíveis");
            espanol_Portugues.Add("no se puede agregar mas contenido.", "Não é possível colocar mais conteúdo.");
            espanol_Portugues.Add("alto", "Alto");
            espanol_Portugues.Add("bajo", "Baixo");
            espanol_Portugues.Add("medio", "Médio");
            espanol_Portugues.Add("(v)", "(T)");
            espanol_Portugues.Add("quigen", "QUIGEN");

            espanol_Portugues.Add("calificación", "Pontuação");
            espanol_Portugues.Add("calificación:", "Pontuação:");
            espanol_Portugues.Add("el tanque actualmente se encuentra lleno. si es necesario, active la válvula de escape.", "Atualmente, o tanque está cheio. Para esvaziá-lo, ative a válvula de escape.");

            espanol_Portugues.Add("el tanque actualmenta se encuentra vacío.", "Atualmente, o tanque está vazio.");
            espanol_Portugues.Add("v", "V");
            espanol_Portugues.Add("cancelar", "Cancelar");
            espanol_Portugues.Add("validar", "Validar");

            espanol_Portugues.Add("regresar", "RETORNAR");
            espanol_Portugues.Add("inicio de sesión", "Conecte-Se");
            espanol_Portugues.Add("ingresar", "ENTRAR");
            espanol_Portugues.Add("reporte", "RELATÓRIO");
            //test
        }
    }
}
