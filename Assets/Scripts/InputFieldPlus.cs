﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UIPlus
{
    public class InputFieldPlus : InputField
    {
        private string textoInicial;

        protected override void Start()
        {
            base.Start();
            onEndEdit.AddListener(MostrarTexto);
            textoInicial = text;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
			if (!text.Equals (string.Empty)) {
				textoInicial = text;
			}
            //text = "";
            //placeholder.GetComponent<Text>().text = "";
        }

        private void MostrarTexto(string argTextoInputField)
        {
            if (argTextoInputField.Equals(string.Empty))
                text = textoInicial;
        }
    } 
}
