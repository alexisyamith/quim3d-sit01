﻿//Funcinamiento Pdf
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if !UNITY_WEBGL
using iTextSharp.text;
using iTextSharp.text.pdf;
#endif
using UnityEngine;
using System.Text;
using SimpleJSON;
using UnityEngine.UI;
using NSCapturaPantalla;
using Prime31;
using NSUtilities;

/// <summary>
/// Clase encargada de crear el reporte del PDF
/// </summary>
public class Reporte : MonoBehaviour
{
    //Nombres
    private string nombreGrafica;
    private string nombrePDF;

    //Funcinamiento Pdf
    private string path;
    //private string _data = string.Empty;	
    private float maxTiempo = 3;
    private string inBase64Pt1 = "";
    private string inBase64Pt2 = "";
#if !UNITY_WEBGL
    private iTextSharp.text.Image pic1;
    private Document doc = new Document();
#endif
    public string usuario;
    public string text_curso;
    public float curso;
    public string nombreSituación;
    public string fecha;

    //Tiro Parabolico
    public Texture2D graficaLanzamiento;
    public string velocidadInicial;
    public string angulo;
    public string velocidadX;
    public string velocidadY;
    public string segundosSesion;
    public string minutosSesion;
    public string alturaMaximaY;
    public string distanciaObtenida;
    public string intentos;
    public List<string> preguntas = new List<string>();
    public List<bool> respuestas = new List<bool>();
    public Dictionary<bool, string> respuestaEspanol = new Dictionary<bool, string>();
    public Pantalla pantalla;
    public string reporteDastosUsuario;
    public string labelDastosUsuario;
    public string textoPreguntas;
    public RawImage[] graficasReporte;
    public bool nuevoDocumento = true;
    private bool practicalibre = false;
    public Camera CamaraGUI;
    public GameObject panelReporte;
    public RectTransform spriteHojaFondoReporte;
    private int borde = 4;
    public GameObject botonTerminarReporte;
    public GameObject[] botonesMovilesReporte;

    public decimal calificacion;
    public string user_report = "";
    public string lab_code_report = "";
    public bool aula_mode = false;
    private byte[] fileBytesAula;
    private string pdf_name_aula = "";

    public string checkIdioma = "";
    public string SimboloVerdadero = "";
    public string SimboloFalso = "";

    public bool _checkLanguage;

    public ControlIdiomas controlIdiomas;

    private void Start()
    {
        //Simbolos Preguntas
        if (_checkLanguage)
        {
            Debug.Log("Sel - TRUE");
        }
        else
        {
            Debug.Log("Sel - FALSE");
            CambiarSimbolosPreguntas();
        }
    }

    public void CambiarSimbolosPreguntas()
    {
        //Check del Idioma
        checkIdioma = (controlIdiomas.idioma).ToString();
        switch (checkIdioma)
        {
            case "Ingles":
                Debug.Log("R-Ing");
                SimboloVerdadero = "T";
                SimboloFalso = "F";
                break;
            case "Espanol":
                Debug.Log("R-Esp");
                SimboloVerdadero = "V";
                SimboloFalso = "F";
                break;
            case "Portugues":
                Debug.Log("R-Por");
                SimboloVerdadero = "V";
                SimboloFalso = "F";
                break;
        }

        respuestaEspanol.Add(true, SimboloVerdadero);
        respuestaEspanol.Add(false, SimboloFalso);
    }

    public void GuardarDatosSesion(string _usuario, string _curso, string _nombreSituacion)
    {
        usuario = _usuario;
        Single.TryParse(_curso, out curso);
        nombreSituación = _nombreSituacion;
    }

    public void GuardarPreguntas(List<string> _preguntasUsuario)
    {
        preguntas.Clear();
        for (int i = 0; i < _preguntasUsuario.Count; i++)
        {
            preguntas.Add(_preguntasUsuario[i]);
        }
    }

    public void GuardarRespuestas(List<bool> respuestasUsuario)
    {
        respuestas = respuestasUsuario;
    }

    public void GuardarNombrePDF(string _nombrePDF)
    {
        nombrePDF = _nombrePDF;
    }

    public void GuardarTablaDatos(List<string> textos, List<string> datos)
    {
        //Realiza doble salto de linea excepto en la ultima
        string espacios = "\n\n";
        int l = 1;
        textoPreguntas = "";
        foreach (string _preguntas in preguntas)
        {
            if (l > 4)
                espacios = "";
            textoPreguntas += l + ")." + _preguntas.Split(':')[0] + "    " + respuestaEspanol[respuestas[l - 1]] + espacios /*"\n\n"*/;
            l++;
        }
        pantalla.ActualizarTablaDatos(textos, datos, textoPreguntas);
    }

    public void ActivarBotonesReporte(bool activar)
    {
        botonTerminarReporte.SetActive(activar);

        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        {
            foreach (GameObject g in botonesMovilesReporte)
            {
                g.SetActive(activar);
               
                Debug.Log("ActivarBotonesReporte");
            }
        }
    }

    public IEnumerator CrearImagen(bool _nuevoDocumento = true, bool _practicalibre = false)
    {
        ActivarBotonesReporte(false);
        nuevoDocumento = _nuevoDocumento;
        practicalibre = _practicalibre;

        yield return new WaitForEndOfFrame();

        // Copiar el rectangulo del widget, en una textura
        Texture2D tex = CapturaPantalla.CapturarImagenRectTransform(spriteHojaFondoReporte);
#if !UNITY_WEBGL
        // Encode texture into PNG
        byte[] bytes = tex.EncodeToPNG();
#endif

        // For testing purposes, also write to a file in the project folder
        // esto no funciona en web 
        // Si es web player , enviar la imagen al navegador 
        if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
        {
            ActivarBotonesReporte(true);
        }

        if ( Application.platform == RuntimePlatform.WebGLPlayer)
        {
            // Enviar la textura para se codificada como JPG y luego enviarla al html para ser descargada con jsPDF 
            StartCoroutine(BajarImagenParaWebPlayer(tex));
        }
        else
        {
            // Si es Android o IOS , descargar la imagen y luego crear el pdf con esa imagen , y lanzarlo
#if !UNITY_WEBGL
            File.WriteAllBytes(Application.persistentDataPath + "/reporte.png", bytes);
            int _ancho = Convert.ToInt32(tex.width);
            createPDFMoviles("texto ", nombrePDF, _ancho, tex.height);
#endif
        }
        ActivarBotonesReporte(true);
    }

    static public Texture2D CopiarWidgetDeRenderTexture(RectTransform widget, Camera cam, RenderTexture rt)
    {
        //Encontrar el rectangulo para leer los pixeles
        Vector3 topLeft = cam.WorldToScreenPoint (widget.position);
        Vector3 bottomRight = cam.WorldToScreenPoint ((Vector2) widget.position+ widget.sizeDelta);
		
		float panelWidth = bottomRight.x - topLeft.x;
		float panelHeight = bottomRight.y - topLeft.y;
		
		int inicioX = Mathf.CeilToInt(rt.width/2.0f - panelWidth/2.0f);
		int inicioY = Mathf.CeilToInt(rt.height/2.0f - panelHeight/2.0f);
		
		// Leer los pixeles de toda la pantalla en otro RenderTexture
		int wReadTex  = Mathf.FloorToInt(panelWidth);
		int hReadTex =  Mathf.FloorToInt(panelHeight);
		
		// Crear el RenderTexture y llenarlo de blanco
		Texture2D tex = new Texture2D(wReadTex, hReadTex, TextureFormat.RGB24, false);
		
		Color fillColor = new Color(1, 1, 1);
		Color[] fillColorArray =  tex.GetPixels();

		for(var i = 0; i < fillColorArray.Length; ++i){
			fillColorArray[i] = fillColor;
		}

		tex.SetPixels( fillColorArray );
		tex.Apply();
		
		RenderTexture.active = rt;
		tex.ReadPixels(new Rect(inicioX + 3, inicioY + 3, wReadTex - 4 , hReadTex -4 ), 0, 0, false);
		tex.Apply ();
        return tex;
    }

    void DefinirPath(string fileName)
    {
        string add = practicalibre ? "_pLibre" : "";

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            path = Application.persistentDataPath + "/" + fileName + add + ".pdf";
        }
        else
        {
            path = Application.dataPath + "/../" + fileName + add + ".pdf";
        }
    }

    void AbrirPdf_StandAlone()
    {
        if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
            string pdfRuta = "file:///" + path;
            pdfRuta = Uri.EscapeUriString(pdfRuta);
            pdfRuta = pdfRuta.Replace(" ", "%20");
            print(pdfRuta);
            Application.OpenURL(pdfRuta);
        }
    }

    private void createPDFMoviles(string fileText, string fileName, int ancho, int alto)
    {
        print("el nombre del pdf es " + fileName);
        path = Application.persistentDataPath + "/" + fileName + ".pdf";
        DefinirPath(fileName);

        pdf_name_aula = fileName + ".pdf";

        if (nuevoDocumento)
        {
            print("Path del documento es " + path);
#if !UNITY_WEBGL
            Rectangle rectangulo = new Rectangle(ancho, alto);
            doc = new Document(rectangulo);
            doc.SetMargins(0, 0, 0, 0);
            PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
            doc.Open();
            pic1 = iTextSharp.text.Image.GetInstance(Application.persistentDataPath + "/reporte.png", true);
            doc.Add(pic1);
#endif

            if (practicalibre)
            {
                CerrarDocumento();
            }
        }
        else
        {
#if !UNITY_WEBGL
            pic1 = iTextSharp.text.Image.GetInstance(Application.persistentDataPath + "/reporte.png", true);
            doc.Add(pic1);
#endif
            CerrarDocumento();
        }
    }

    void CerrarDocumento()
    {
#if !UNITY_WEBGL
        doc.Close();
#endif

        ActivarBotonesReporte(true);
        AbrirPdf_StandAlone();

        if (aula_mode == true && pantalla.var_offline == false)
        {
            FileStream stream = File.OpenRead(path);
            fileBytesAula = new byte[stream.Length];
            stream.Read(fileBytesAula, 0, fileBytesAula.Length);
            stream.Close();

            Debug.Log(fileBytesAula.Length);
            Debug.Log(fileBytesAula);

            leerAulaScore();
            Debug.Log("CerrarDocumento");
        }
    }

    private void leerAulaScore()
    {
        string _data = "{\"user\":\"" + user_report + "\",\"labCode\":\"" + lab_code_report + "\"}";

        byte[] bytesToEncode = Encoding.UTF8.GetBytes(_data);
        string encodedText = Convert.ToBase64String(bytesToEncode);

        string _url = pantalla.simulador.url_aula + "/externals/get_lab?data=" + encodedText;

        Debug.Log(_url);

        WWW wwwAulaGet = new WWW(_url);
        StartCoroutine(WaitForRequestAulaGet(wwwAulaGet));
    }

    private void set_aula_score()
    {
        string _url = pantalla.simulador.url_aula + "/externals/put_lab";
        float nota = pantalla._cal / 5;
        nota = nota * 100f;
        string _date = DateTime.Now.ToString("yyyy/MM/dd");

        WWWForm form = new WWWForm();
        string _params = "{\"user\":\"" + user_report + "\",\"labCode\":\"" + lab_code_report + "\",\"attempts\":" + pantalla.simulador.simuladorActivo.intentos.ToString() + ",\"delivery_date\":\"" + _date + "\",\"delivery_time\":\"" + pantalla.ref_Tiempo.text.ToString() + "\",\"app_score\":" + nota.ToString() + "}";
        Debug.Log(_params);

        form.AddField("data", _params);
        Debug.Log("nombre del pdf"+ pdf_name_aula);
        form.AddBinaryData("report_file", fileBytesAula, nombrePDF+".pdf", "application/pdf");

        WWW wwwAulaPut = new WWW(_url, form);
        StartCoroutine(WaitForRequestAulaPut(wwwAulaPut));
    }

    IEnumerator WaitForRequestAulaGet(WWW www)
    {
        yield return www;

        if (www.error == null)
        {
            byte[] decodedBytes = Convert.FromBase64String(www.text);
            string decodedText = Encoding.UTF8.GetString(decodedBytes);
            Debug.Log(decodedText);
            var _data = JSON.Parse(decodedText);

            if (_data["state"] != null)
            {
                if (_data["state"].Value == "true")
                {
                    if (_data["res_code"] != null)
                    {
                        switch (_data["res_code"].Value)
                        {
                            case "USER_NOT_FOUND":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The username is not registered. Please talk to your teacher.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("El usuario no se encuentra registrado. Por favor comuníquese con su profesor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("O usuário não está registrado. Por favor, entre em contato com seu professor.");
                                        break;
                                }
                                break;
                            case "LAB_NOT_ASSIGNED":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The laboratory is not assigned to the student. Please ask your teacher.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("O laboratório não está liberado para o estudante. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                }
                                break;
                            case "LAB_NOT_FOUND":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The practice could not be found. Please talk to your teacher.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("No se encuentra la práctica. Por favor comuníquese con su profesor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Prática não encontrada. Por favor, entre em contato com seu professor.");
                                        break;
                                }
                                break;
                            case "STATUS_OK":
                                if (_data["lab_state"].Value == "0")
                                {
                                    Debug.Log("OK");
                                    set_aula_score();
                                }
                                else
                                {
                                    pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                    pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                    pantalla.btnContinuar.gameObject.SetActive(false);
                                    pantalla.botonesAulaUno.SetActive(true);
                                    botonTerminarReporte.SetActive(false); //desactivar boton terminar reporte
                                    switch (pantalla.IdiomaActual)
                                    {
                                        case "Ingles":
                                            pantalla.textoMensaje.text = controlIdiomas.Traductor("You have already registered a grade and a laboratory report for this practice. The data could not be saved. Please talk to your teacher.");
                                            break;
                                        case "Espanol":
                                            pantalla.textoMensaje.text = controlIdiomas.Traductor("Usted ya ha registrado una calificación y un reporte de laboratorio para esta práctica. Los datos generados no pudieron ser guardados. Por favor comuníquese con su profesor.");
                                            break;
                                        case "Portugues":
                                            pantalla.textoMensaje.text = controlIdiomas.Traductor("Você já registrou uma pontuação e um relatório de laboratório para esta prática. Os dados gerados não puderam ser armazenados. Por favor, entre em contato com seu professor.");
                                            break;
                                    }
                                }
                                break;
                            case "DB_EXCEPTION":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Database error. Please contact your provider.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Error en la base de datos. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Erro na base de dados. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                }
                                break;
                        }
                    }
                    else
                    {
                        pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                        pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                        pantalla.btnContinuar.gameObject.SetActive(false);
                        pantalla.botonesAulaUno.SetActive(true);
                        switch (pantalla.IdiomaActual)
                        {
                            case "Ingles":
                                pantalla.textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                                break;
                            case "Espanol":
                                pantalla.textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                                break;
                            case "Portugues":
                                pantalla.textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                                break;
                        }
                    }
                }
                else
                {
                    pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                    pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                    pantalla.btnContinuar.gameObject.SetActive(false);
                    pantalla.botonesAulaUno.SetActive(true);
                    switch (pantalla.IdiomaActual)
                    {
                        case "Ingles":
                            pantalla.textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                            break;
                        case "Espanol":
                            pantalla.textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                            break;
                        case "Portugues":
                            pantalla.textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                            break;
                    }
                }
            }
            else
            {
                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                pantalla.btnContinuar.gameObject.SetActive(false);
                pantalla.botonesAulaUno.SetActive(true);
                switch (pantalla.IdiomaActual)
                {
                    case "Ingles":
                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                        break;
                    case "Espanol":
                        pantalla.textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                        break;
                    case "Portugues":
                        pantalla.textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                        break;
                }
            }

        }
        else
        {
            pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
            pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
            pantalla.btnContinuar.gameObject.SetActive(false);
            pantalla.botonesAulaUno.SetActive(true);
            switch (pantalla.IdiomaActual)
            {
                case "Ingles":
                    pantalla.textoMensaje.text = controlIdiomas.Traductor("It has not been possible to connect to the teacher's device. Please try to generate the report again or talk to your teacher. If the problem persists, please save a copy of your laboratory report.");
                    break;
                case "Espanol":
                    pantalla.textoMensaje.text = controlIdiomas.Traductor("No se ha podido establecer la conexión con el equipo del profesor. Intente generar de nuevo el reporte de laboratorio o comuníquese con su profesor. Si el problema persiste por favor guarde una copia de su reporte de laboratorio.");
                    break;
                case "Portugues":
                    pantalla.textoMensaje.text = controlIdiomas.Traductor("Não foi possível estabelecer a conexão com o equipamento do professor. Tente gerar o relatório de laboratório novamente ou entre em contato com seu professor. Se o problema continuar, por favor, salve uma cópia do seu relatório de laboratório.");
                    break;
            }
        }
    }

    IEnumerator WaitForRequestAulaPut(WWW www)
    {
        yield return www;

        if (www.error == null)
        {
            byte[] decodedBytes = Convert.FromBase64String(www.text);
            string decodedText = Encoding.UTF8.GetString(decodedBytes);
            Debug.Log(decodedText);
            var _data = JSON.Parse(decodedText);

            if (_data["state"] != null)
            {
                if (_data["state"].Value == "true")
                {
                    if (_data["res_code"] != null)
                    {
                        switch (_data["res_code"].Value)
                        {
                            case "LAB_INSERTED":
                                Debug.Log("succesful insert");
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                botonTerminarReporte.SetActive(false); //desactivar boton terminar reporte
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The laboratory report and the data from the practice have been sent to your teacher.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("El reporte de laboratorio y los datos de esta práctica han sido enviados a su profesor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("O relatório de laboratório e os dados da prática foram enviados ao seu professor.");
                                        break;
                                }
                                break;
                            case "LAB_NOT_ASSIGNED":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The laboratory is not assigned to the student. Please ask your teacher.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("O laboratório não está liberado para o estudante. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                }
                                break;
                            case "LAB_NOT_FOUND":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The practice could not be found. Please talk to your teacher.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("No se encuentra la práctica. Por favor comuníquese con su profesor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Prática não encontrada. Por favor, entre em contato com seu professor.");
                                        break;
                                }
                                break;
                            case "LAB_UPDATED":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                botonTerminarReporte.SetActive(false); //desactivar boton terminar reporte
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The laboratory report and the data from the practice have been updated and sent to your teacher.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("El reporte de laboratorio y los datos de esta práctica han sido actualizados y enviados a su profesor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("O relatório de laboratório e os dados da prática foram atualizados e enviados ao seu professor.");
                                        break;
                                }
                                break;
                            case "DB_EXCEPTION":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Database error. Please contact your provider.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Error en la base de datos. Por favor comuníquese con su proveedor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Erro na base de dados. Por favor, entre em contato com seu fornecedor.");
                                        break;
                                }
                                break;
                            case "LICENSE_EXPIRED":
                                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                                pantalla.btnContinuar.gameObject.SetActive(false);
                                pantalla.botonesAulaUno.SetActive(true);
                                switch (pantalla.IdiomaActual)
                                {
                                    case "Ingles":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("There is a problem with your licence, please contact your provider.");
                                        break;
                                    case "Espanol":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Hay un problema con la licencia del gestor de aula, por favor comuníquese con el proveedor.");
                                        break;
                                    case "Portugues":
                                        pantalla.textoMensaje.text = controlIdiomas.Traductor("Há um problema com a licença do gestor da sala, por favor, entre em contato com o fornecedor.");
                                        break;
                                }
                                break;
                        }
                    }
                    else
                    {
                        pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                        pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                        pantalla.btnContinuar.gameObject.SetActive(false);
                        pantalla.botonesAulaUno.SetActive(true);
                        switch (pantalla.IdiomaActual)
                        {
                            case "Ingles":
                                pantalla.textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                                break;
                            case "Espanol":
                                pantalla.textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                                break;
                            case "Portugues":
                                pantalla.textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                                break;
                        }
                    }
                }
                else
                {
                    pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                    pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                    pantalla.btnContinuar.gameObject.SetActive(false);
                    pantalla.botonesAulaUno.SetActive(true);
                    switch (pantalla.IdiomaActual)
                    {
                        case "Ingles":
                            pantalla.textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                            break;
                        case "Espanol":
                            pantalla.textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                            break;
                        case "Portugues":
                            pantalla.textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                            break;
                    }
                }
            }
            else
            {
                pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
                pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
                pantalla.btnContinuar.gameObject.SetActive(false);
                pantalla.botonesAulaUno.SetActive(true);
                switch (pantalla.IdiomaActual)
                {
                    case "Ingles":
                        pantalla.textoMensaje.text = controlIdiomas.Traductor("The server response is invalid. Please contact your provider.");
                        break;
                    case "Espanol":
                        pantalla.textoMensaje.text = controlIdiomas.Traductor("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                        break;
                    case "Portugues":
                        pantalla.textoMensaje.text = controlIdiomas.Traductor("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                        break;
                }
            }

        }
        else
        {
            pantalla.AbrirPanel(pantalla.panelInicial.gameObject);
            pantalla.AbrirPanel(pantalla.panelMensajes.gameObject);
            pantalla.btnContinuar.gameObject.SetActive(false);
            pantalla.botonesAulaUno.SetActive(true);
            switch (pantalla.IdiomaActual)
            {
                case "Ingles":
                    pantalla.textoMensaje.text = controlIdiomas.Traductor("It has not been possible to connect to the teacher's device. Please try to generate the report again or talk to your teacher. If the problem persists, please save a copy of your laboratory report.");
                    break;
                case "Espanol":
                    pantalla.textoMensaje.text = controlIdiomas.Traductor("No se ha podido establecer la conexión con el equipo del profesor. Intente generar de nuevo el reporte de laboratorio o comuníquese con su profesor. Si el problema persiste por favor guarde una copia de su reporte de laboratorio.");
                    break;
                case "Portugues":
                    pantalla.textoMensaje.text = controlIdiomas.Traductor("Não foi possível estabelecer a conexão com o equipamento do professor. Tente gerar o relatório de laboratório novamente ou entre em contato com seu professor. Se o problema continuar, por favor, salve uma cópia do seu relatório de laboratório.");
                    break;
            }
        }
    }

    public void VisualizarPDF()
    {
        print("visualizar");
#if UNITY_ANDROID
		print("path: " + path);
		Application.OpenURL(path);	
#endif

#if UNITY_IPHONE
		print("path2: " + path);
		EtceteraBinding.showWebPage ( path , true );
#endif
    }

    public void Enviar_PDF_Correo()
    {
#if UNITY_ANDROID
		EtceteraAndroid.showEmailComposer( "", "Reporte", "Reporte", false ,   path );
#endif

#if UNITY_IPHONE
		EtceteraBinding.showMailComposerWithAttachment( path , "application/pdf" , "Reporte" , "direccion" ,"Reporte " , "Reporte " , false);
#endif
    }

    private IEnumerator BajarImagenParaWebPlayer(Texture2D reporte)
    {
        var jpgEncoder = new JPGEncoder(reporte, 100);
        float tiempoTotal = 0;
        while (!jpgEncoder.isDone && tiempoTotal < maxTiempo)
        {
            yield return null;
            tiempoTotal += Time.deltaTime;
        }
        if (jpgEncoder.isDone)
        {
            byte[] imgJPG = jpgEncoder.GetBytes();


            if (nuevoDocumento)
            {
                inBase64Pt1 = "data:image/jpeg;base64," + Convert.ToBase64String(imgJPG);
                if (practicalibre)
                {
                    Application.ExternalCall("pdfReporte", inBase64Pt1, "", nombrePDF, aula_mode /*nuevoDocumento*/);
                    Debug.LogError("LogError");
                }
            }
            else
            {
                inBase64Pt2 = "data:image/jpeg;base64," + Convert.ToBase64String(imgJPG);
                Application.ExternalCall("pdfReporte", inBase64Pt1, inBase64Pt2, nombrePDF, aula_mode /*nuevoDocumento*/);
            }
        }
    }

    public void EnviarReporteWebAula(String PdfWebAula)
    {
        fileBytesAula = Convert.FromBase64String(PdfWebAula);
        leerAulaScore();
    }


}