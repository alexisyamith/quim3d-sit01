
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using UnityEngine.UI;
using NSSeleccionMouse;

/// <summary>
/// Clase general para todos los simuladores
/// </summary>
public class SimuladorGenerico : MonoBehaviour
{
    /// <summary>
    /// Posicion en donde se acomoda la camara para enfocar la unidad/ estacion seleccionada por el usuario
    /// </summary>
    public Transform posFoco;
    /// <summary>
    /// Clase que contiene toda la funcionalidad del reporte pdf
    /// </summary>
    public Reporte reporte;

    /// <summary>
    /// Titulo del simulador actual.
    /// </summary>
    public string titulo;
    /// <summary>
    /// Texto que explica la mision en la practica libre.
    /// </summary>
    public string textoMisionPracticaLibre;
    /// <summary>
    /// Texto que explica la mision en el reto.
    /// </summary>
    public string textoMisionReto;
    /// <summary>
    /// Texto que explica como ejecutar la practica libre del simulador actual.
    /// </summary>
    public string textoComoJugarPracticaLibre;
    /// <summary>
    /// Texto que explica como ejecutar el reto del simulador actual.
    /// </summary>
    public string textoComoJugarReto;
    /// <summary>
    /// Instrucciones adiciones que aparecen en la misma pantalla donde esta la calculadora
    /// </summary>
    public string textoInstruccionesPantalla;
    /// <summary>
    /// Texto que le dice al usuario si aprobo el ejercicio
    /// </summary>
    public string textoMensajeAprobo;
    /// <summary>
    /// Texto que le dice al usuario si reprobo el ejercicio
    /// </summary>
    public string textoMensajeNoAprobo;
    /// <summary>
    /// Texto de preguntas que ayudan al usuario a orientarse en el ejercicio
    /// </summary>
    public string preguntasOrientadoras;
    /// <summary>
    /// Situacion que se esta resolviendo actualmente en el simulador
    /// </summary>
    public string situacion;
    /// <summary>
    /// Unidad a la que pertenece el simulador
    /// </summary>
    public string unidad;
    /// <summary>
    /// Nombre de la grafica de la unidad actual
    /// </summary>
    public string nombreGrafica;
    /// <summary>
    /// Nombre del archivo pdf con el reporte
    /// </summary>
    public string nombrePDF;
    /// <summary>
    /// Para saber si el usuario ejecuto mas de un intento para resolver un ejercicio
    /// </summary>
    public bool ejecutoVariosEjercicios = false;
    /// <summary>
    /// Mini pantalla que se anima para mostrar que se abrira una pantalla mucho mas grande
    /// </summary>
    public GameObject pantalla3D;
    /// <summary>
    /// Punto desde donde sale la pantalla 3D que contiene la calculadora como una de sus opciones
    /// </summary>
    public GameObject compartimientoMesa3D;
    /// <summary>
    /// Cantidad de intentos hechos por el usuario para superar el reto del simulador, estos intentos se reinician cada vez que el usuario entra a un simulador o preciona el boton reiniciar
    /// </summary>
    public int intentos;
    /// <summary>
    /// Segundos que el usuario a permanecido resolviendo un simulador
    /// </summary>
    public float segundosSesion;
    /// <summary>
    /// Text donde se muestra el tiempo actual de la sesion
    /// </summary>
    public Text hud_Tiempo;
    /// <summary>
    /// Text hubicado en la pantalla que contiene la opcion de calculadora, donde se muestra el tiempo actual de la sesion
    /// </summary>
    public Text pantalla_Tiempo;
    /// <summary>
    /// Lista de preguntas del reto
    /// </summary>
    public List<string> listaPreguntas = new List<string>();
    /// <summary>
    /// Clase que controla las animaciones y cambio de color del boton que muestra la pantalla que contiene la grafica, calculadora etc
    /// </summary>
    public AnimacionBotones botonPantalla;
    /// <summary>
    /// Clase que controla algunos elementos del simulador
    /// </summary>
    public Simulador simulador;
    /// <summary>
    /// �El usuario aprobo el ejercicio?
    /// </summary>
    public bool aproboEjercicio = false;
    /// <summary>
    /// Nombres de los datos que se mostraran al final en el pdf
    /// </summary>
    public List<string> tablaDatosTextos = new List<string>();
    /// <summary>
    /// textos de la cantidad asociada a cada uno de los nombres de los datos que se mostraran en el pdf
    /// </summary>
    public List<string> tablaDatos = new List<string>();

    /// <summary>
    /// Panel en donde se muestran todos los datos de la unidad actual
    /// </summary>
    public GameObject entradaDatos;
    /// <summary>
    /// Panel en donde se muestran los indicadores de la unidad actual
    /// </summary>
    public GameObject indicadores;
    /// <summary>
    /// Panel de la pantalla donde esta la calculadora, que contiene los datos actuales de la unidad
    /// </summary>
    public GameObject datosPantalla;
    /// <summary>
    /// Panel que contiene las ecuaciones de la unidad
    /// </summary>
    public GameObject ecuaciones;
    /// <summary>
    /// Para saber si el usuario esta en practica libre, si esta en practica libre, lo mas seguro es que se muestren mas opciones pertenecientes a este modo
    /// </summary>
    public bool practicaLibre;
    /// <summary>
    /// para saber si el tanque esta disponible, porque puede que este se encuentre lleno
    /// </summary>
    public bool tanqueDisponible = true;
    /// <summary>
    /// El tanque esta vacio?
    /// </summary>
    public bool tanqueVacio = true;
    /// <summary>
    /// Mensaje que se muestra cuando el tanque esta lleno
    /// </summary>
    public string msjTanqueLleno = "El tanque actualmente se encuentra lleno si es necesario active la valvula de escape.";
    /// <summary>
    /// Mensaje que se muestra cuando el tanque esta vacio
    /// </summary>
    public string msjTanqueVacio = "El tanque actualmente se encuentra vacio.";
    /// <summary>
    /// Lista en donde se guardan las preguntas cargadas desde un archivo
    /// </summary>
    public List<string> refExternalQuestions = new List<string>();
    /// <summary>
    /// Array en donde se guardan las preguntas cargadas desde un archivo
    /// </summary>
    public string[] externalQuestions = new string[4];
    /// <summary>
    /// Clase que controla el cambio de idiomas
    /// </summary>
    public ControlIdiomas controlIdiomas;
    /// <summary>
    /// El usuario inicio la experimentacion en la unidad?
    /// </summary>
    public bool usuarioEntroEstacion = false;

    [SerializeField, Tooltip("Clase que ejecuta la seleccion con el mouse")]
    protected SeleccionMouse refSeleccionMouse;

    public virtual void ActualizarDatosReporte() { }
    public virtual void BtnIniciar(bool _practica) { }
    public virtual float[] ObtenerDatosGrafica()
    {
        return null;
    }

    /// <summary>
    /// Reinicia el tiempo de la session
    /// </summary>
    public void ReiniciarTiempo()
    {
        segundosSesion = 0;
        hud_Tiempo.text = "0";
        pantalla_Tiempo.text = "0";
    }

    /// <summary>
    /// Cuenta el tiempo de la session y lo asigna a la interfaz
    /// </summary>
    public void ContarTiempoSession()
    {
        segundosSesion += Time.deltaTime;
        TimeSpan t = TimeSpan.FromSeconds(segundosSesion);
        string tiempoConvertido = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds);
        hud_Tiempo.text = tiempoConvertido;
        pantalla_Tiempo.text = tiempoConvertido;
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void NoContarTiempo()
    {

    }

    /// <summary>
    /// Key para cargar un archivo con todos los textos correspondientes a este simulador
    /// </summary>
    public string keyNameJson = "reactor";

    /// <summary>
    /// funcion del editor que carga todos los datos de este simulador
    /// </summary>
    [ContextMenu("CargarDatosJson")]
    public void CargarTextos()
    {
        CargarTextosJson(keyNameJson, this, listaPreguntas);
    }

    /// <summary>
    /// Funcion que carga todos los textos y preguntas desde un archivo
    /// </summary>
    /// <param name="keySimulador">Key del simulador del cual se desean cargar los datos</param>
    /// <param name="simulador">Clase para asignarle los datos cargados</param>
    /// <param name="pregutasEva">Lista en donde se almacenan las preguntas de evaluacion</param>
    void CargarTextosJson(string keySimulador, SimuladorGenerico simulador, List<string> pregutasEva)
    {
        ControlIdiomas controlIdiomas = FindObjectOfType<ControlIdiomas>();
        TextAsset rutaRes = null;

        if (controlIdiomas.idioma == ControlIdiomas.Idioma.Ingles)
        {
            print("cargar ingles");
            rutaRes = Resources.Load("textos_simulador_3D_quimica_ingles") as TextAsset;
        }

        if (controlIdiomas.idioma == ControlIdiomas.Idioma.Espanol)
        {
            Debug.Log("cargar espanol...");
            rutaRes = Resources.Load("textos_simulador_3D_quimica") as TextAsset;
        }

        if (controlIdiomas.idioma == ControlIdiomas.Idioma.Portugues)
        {
            rutaRes = Resources.Load("textos_simulador_3D_quimica_portugues") as TextAsset;
        }

        string rutaRes_String = rutaRes.text;//Se carga todo el texto del archivo pero no se puede leer selectivamente
        Debug.Log(rutaRes_String);

        //string to stream
        byte[] byteArrray = Encoding.UTF8.GetBytes(rutaRes_String);//convierto el texto a bytes
        MemoryStream Res_stream = new MemoryStream(byteArrray);//creo un memory stream con los bytes

        var sr_resource = new StreamReader(Res_stream);//creo un stream de lectura
        string datos = sr_resource.ReadToEnd();//leo todos los datos del stream

        sr_resource.Close();//cierro el stream delectura

       //JSONObject obj = new JSONObject(datos);//convierto los datos a un JSON

        //Debug.Log(obj);
       
        SimpleJSON.JSONNode tmpJsonObject = SimpleJSON.JSON.Parse(datos);
        //Debug.Log("nuevo:   "+ tmpJsonObject.ToString());
        SimpleJSON.JSONNode datosJson = tmpJsonObject[keySimulador];
        //JSONObject datosJson = obj.GetField(keySimulador);

        if (datosJson == null)
        {
            Debug.LogError("Json mal cargado : " + keySimulador);
            return;
        }

       
        simulador.textoComoJugarReto = datosJson["comoJugar"].Value.Replace("\"", string.Empty);
       // simulador.textoComoJugarReto = simulador.textoComoJugarReto.Replace("\t","  ");

        simulador.textoMisionReto = datosJson["misionJuego"].Value.Replace("\"", string.Empty);
        //simulador.textoMisionReto = simulador.textoMisionReto.Replace("\t","  ");

        simulador.textoComoJugarPracticaLibre = datosJson["comoJugarPracticaLibre"].Value.Replace("\"", string.Empty);
        //simulador.textoComoJugarPracticaLibre = simulador.textoComoJugarPracticaLibre.ToString().Replace("\t", "  ");


        simulador.textoMisionPracticaLibre = datosJson["misionJuegoPracticaLibre"].Value.Replace("\"", string.Empty);
       // simulador.textoMisionPracticaLibre = simulador.textoMisionPracticaLibre.Replace("\t", "  ");

        simulador.textoInstruccionesPantalla = datosJson["instrucciones"].Value.Replace("\"", string.Empty);
        //simulador.textoInstruccionesPantalla = simulador.textoInstruccionesPantalla.Replace("\t", "  ");

        simulador.titulo = datosJson["titulo"].Value.Replace("\"", string.Empty);
        //simulador.titulo = simulador.titulo.ToString().Replace("\t", "  ");


        simulador.situacion = datosJson["situacion"].Value.Replace("\"", string.Empty);
       // simulador.situacion = simulador.situacion.ToString().Replace("\t", "  ");

        simulador.unidad = datosJson["unidad"].Value.Replace("\"", string.Empty);
       //simulador.unidad = simulador.unidad.ToString().Replace("\t", "  ");

        simulador.textoMensajeAprobo = datosJson["textoMensajeAprobo"].Value.Replace("\"", string.Empty);
       //simulador.textoMensajeAprobo = simulador.textoMensajeAprobo.ToString().Replace("\t", "  ");

        simulador.textoMensajeNoAprobo = datosJson["textoMensajeNoAprobo"].Value.Replace("\"", string.Empty);
       //simulador.textoMensajeNoAprobo = simulador.textoMensajeNoAprobo.ToString().Replace("\t", "  ");

        simulador.nombreGrafica = datosJson["nombreGrafica"].Value.Replace("\"", string.Empty);
       //simulador.nombreGrafica = simulador.nombreGrafica.ToString().Replace("\t", "  ");

        simulador.nombrePDF = datosJson["nombrePdf"].Value.Replace("\"", string.Empty);
       //simulador.nombrePDF = simulador.nombrePDF.ToString().Replace("\t", "  ");

        simulador.preguntasOrientadoras = "";

        for (int i = 0; i < 5; i++)
        {//cargo cada una de las preguntas que ense�an
            string key = "preguntaOrientadora" + (i + 1).ToString();

            if (!datosJson[key])
            {
                Debug.LogError("No existe el campo " + key);
            }
            else
            {
                simulador.preguntasOrientadoras += (i + 1).ToString() + ". " + datosJson[key].Value.Replace("\"", string.Empty) + "\n";//asigno el numero de la pregunta y la pregunta
            }
        }

        for (int i = 0; i < 5; i++)
        {//cargo cada una de las preguntas de avaluacion
            string key = "preguntaEvaluacion" + (i + 1).ToString();
            string pregunta = datosJson[key].Value.Replace("\"", string.Empty);
            pregutasEva[i] = pregunta;
        }
    }

    public virtual void MostrarPantalla()
    {

    }
}