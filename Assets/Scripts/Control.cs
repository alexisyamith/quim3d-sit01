﻿using UnityEngine;

/// <summary>
/// Controla los movimientos de camara y la ubicacion de la camara en los puntos especificos de cada mesa.
/// </summary>
public class Control : MonoBehaviour
{
    /// <summary>
    /// referencia a la camara principal 
    /// </summary>
    public GameObject camaraFPS;
    /// <summary>
    /// Referencia a la camara que se activa frente al simulador
    /// </summary>
    public GameObject camaraSimulador;
    /// <summary>
    /// Referencia al objeto que representa el jugador
    /// </summary>
    public GameObject controlTouch;
    /// <summary>
    /// Referencia al objeto que controla el movimiento del personaje dentro de la simulacion
    /// </summary>
    public GameObject firstPersonController;
    /// <summary>
    /// Posicion en donde la camara se ubica para enfocar el simulador
    /// </summary>
    private Transform posFocoCamara;
    /// <summary>
    /// Referencia al game object que representa el joystick
    /// </summary>
    public GameObject singleJoystick;    
    /// <summary>
    /// Tiempo que se demora la camara en ubicarse y enfocar el simulador
    /// </summary>
    public float tiempoAcercamiento;
    /// <summary>
    /// Game object del boton que se presiona para salir de la app
    /// </summary>
    public GameObject btnSalirApp;

    // Use this for initialization
    private void Start()
    {
        controlTouch.SetActive(true);

        #if UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS || UNITY_STANDALONE
		   // btnSalirApp.SetActive(true);
        #endif
    }

    /// <summary>
    /// Se ejecuta cuando el usuario toca una mesa
    /// </summary>
    /// <param name="simuladorActivo">Clase del simulador que toco la mesa</param>
    public void TocoMesa(SimuladorGenerico simuladorActivo)
    {
        #if UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS || UNITY_STANDALONE
		   // btnSalirApp.SetActive(false);
        #endif

        SetActiveFirstPersonController(false);
        camaraFPS.SetActive(false);
        camaraSimulador.SetActive(true);

        // 1. Mover la camaraSimulador donde esta la camaraFPS 
        camaraSimulador.transform.position = camaraFPS.transform.position;
        camaraSimulador.transform.rotation = camaraFPS.transform.rotation;
        // 2. Ir a la posicion de foco de esa mesa  

        posFocoCamara = simuladorActivo.posFoco;

        Vector3 posVista = posFocoCamara.transform.position + posFocoCamara.transform.forward * 5;
        iTween.MoveTo(camaraSimulador, iTween.Hash("position", posFocoCamara, "looktarget", posVista, "easeType", "easeInOutCubic", "time", tiempoAcercamiento));
        controlTouch.transform.position = new Vector3(posFocoCamara.position.x, controlTouch.transform.position.y, posFocoCamara.position.z);
    }

    /// <summary>
    /// Mueve la camara nuevamente a la posicion del controlador FPS
    /// </summary>
    public void SalirCamaraSimulador()
    {
        #if UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS || UNITY_STANDALONE
		    btnSalirApp.SetActive(true);
        #endif
        // TODO : se puede poner en el itween? 
        iTween.MoveTo(camaraSimulador, iTween.Hash("position", camaraFPS.transform.position, "easeType", "easeInOutCubic", "time", tiempoAcercamiento, "oncomplete", "EntrarCamaraFPS", "oncompletetarget", gameObject));
        controlTouch.transform.rotation = Quaternion.Euler(camaraSimulador.transform.rotation.eulerAngles);
    }

    /// <summary>
    /// Activa la camara del controlador FPS
    /// </summary>
    private void EntrarCamaraFPS()
    {
        SetActiveFirstPersonController(true);
        camaraSimulador.SetActive(false);
        camaraFPS.SetActive(true);
    }

    /// <summary>
    /// Activa el control de movimiento para el usuario
    /// </summary>
    /// <param name="valor">Activar?</param>
    public void SetActiveFirstPersonController(bool valor)
    {
        /* primero se debe hacer lo siguiente
            1. Llevar la camaraSimulador donde esta la camara personaje 
            2. Desactivar la camaraSimulacion y activar personaje
        */

        controlTouch.GetComponent<FirstPersonControl>().enabled = valor;
        singleJoystick.SetActive(valor);
    }
}