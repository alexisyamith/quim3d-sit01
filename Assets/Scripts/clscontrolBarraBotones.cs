using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class clscontrolBarraBotones : MonoBehaviour
    {
        #region members

        public GameObject BarraMayorNmeroToggles;

        public GameObject BarraMenorNumeroToggles;

        public Toggle botonSimpreactivo;

        public Toggle[] otrosToggles;
        #endregion

        public void Reiniciartoggles()
        {
            botonSimpreactivo.isOn = true;

            foreach (var tmpToggle in otrosToggles)
                tmpToggle.isOn = false;
        }

        public void ActivarBarraMayor(bool orden)
        {
            BarraMayorNmeroToggles.SetActive(orden);
            //BarraMenorNumeroToggles.SetActive(orden == true? false: true);
            BarraMenorNumeroToggles.SetActive(!orden);
        }
    } 
}
