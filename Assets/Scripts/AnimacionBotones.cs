﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Animacion de los botones 3D que se encuentran en las unidades/estaciones de trabajo
/// </summary>
public class AnimacionBotones : MonoBehaviour
{
    /// <summary>
    /// Referencia al game object del boton
    /// </summary>
    public GameObject modelo3D;
    /// <summary>
    /// Direccion hacia donde se mueve cuando esta presionado
    /// </summary>
    public Vector3 movimiento;
    /// <summary>
    /// Material del boton cuando no esta precionado
    /// </summary>
    public Material materialNormal;
    /// <summary>
    /// Material del boton cuando esta precionado
    /// </summary>
    public Material materialIluminado;
    /// <summary>
    /// Posicion inicial del boton
    /// </summary>
    public Vector3 posicionInicial;
    /// <summary>
    /// Posicion cuando el boton es precionado
    /// </summary>
    private Vector3 posicionFinal;
    /// <summary>
    /// Para saber si el boton  se puede volver a presionar
    /// </summary>
    public enum TipoBoton { SCREEN, RESET };
    /// <summary>
    /// Tipo del boton
    /// </summary>
    public TipoBoton _tipoBoton;

    void Start()
    {
        materialNormal = modelo3D.GetComponent<Renderer>().material;
        posicionInicial = modelo3D.transform.position;
        posicionFinal = posicionInicial + movimiento;
    }

    /// <summary>
    /// Evento de click sobre el boton
    /// </summary>
    public void Click(bool activacionNormal = true)
    {
        if (!activacionNormal)
        {
            Normalizar();
            return;
        }

        modelo3D.transform.position = posicionFinal;
        Iluminar();
        if (_tipoBoton == TipoBoton.RESET)
        {
            Simulador.activeBtnReset = false;
            Invoke("Normalizar", 0.5f);
        }
    }

    /// <summary>
    /// Volver a ubicar el boton en su estado normal antes de que sea presionado
    /// </summary>
    public void Normalizar()
    {
        if (modelo3D.transform.position.z != posicionInicial.z)
            modelo3D.transform.position = posicionInicial;
        modelo3D.GetComponent<Renderer>().material = materialNormal;
    }

    /// <summary>
    /// Iluminar el boton para mostrar que fue presionado
    /// </summary>
    private void Iluminar()
    {
        modelo3D.GetComponent<Renderer>().material = materialIluminado;
    }

    /// <summary>
    /// Dibujar limites boton
    /// </summary>
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.2f);
        Collider col = GetComponent<Collider>();
        if (col != null)
            Gizmos.DrawCube(col.bounds.center, col.bounds.size);
    }

    public IEnumerator ActivarBtInstantaneamente()
    {
        //Configurar valores Iniciales (por si presiona botones muy rapido)
        modelo3D.GetComponent<Renderer>().material = materialNormal;
        modelo3D.transform.position = posicionInicial;

        //Realizar movimiento y encendido
        modelo3D.GetComponent<Renderer>().material = materialIluminado;
        modelo3D.transform.position = posicionFinal;

        yield return new WaitForSeconds(0.3f);

        //Configurar valores Iniciales 
        modelo3D.GetComponent<Renderer>().material = materialNormal;
        modelo3D.transform.position = posicionInicial;
    }
}
